from bs4 import BeautifulSoup
import requests

header = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) "
                  "Chrome/80.0.3987.163 Safari/537.36 "
}

r = requests.get("https://www.doviz.com", headers=header)
veriler = BeautifulSoup(r.content, "html.parser")
veriler = veriler.find("div", {"class": "market-data"}).findAll("div", {"class": "item"})

# currencies/tl

def gram():#gram of gold in turkish lira
    gram = veriler[0].find("span", {"class": "value"}).text
    return gram


def dolar():#USD
    dolar = veriler[1].find("span", {"class": "value"}).text
    return dolar

def euro():
    euro = veriler[2].find("span", {"class": "value"}).text
    return euro

def gbp():
    gbp = veriler[3].find("span", {"class": "value"}).text
    return gbp

def borsa():#XU100
    borsa = veriler[4].find("span", {"class": "value"}).text
    return borsa

def bitcoin():
    bitcoin = veriler[5].find("span", {"class": "value"}).text
    bitcoin = bitcoin[1:] # delete "$" first
    return bitcoin

def gumus():#silver ounce
    gumus = veriler[6].find("span", {"class": "value"}).text
    return gumus

# additional cryptocurrencies, not sure that how long they will work but works now

def ethereum():
    e = requests.get("https://www.doviz.com/kripto-paralar/ethereum", headers=header)
    veri = BeautifulSoup(e.content, "html.parser")
    veri = veri.find("div", {"class": "currency-card relative bg-blue-gray-9 rounded-md p-16"})
    ethereum = veri.find("div", {"class": "text-xl font-semibold text-white"}).text
    ethereum = ethereum[1:] # delete "$" first
    return ethereum

def doge():
    e = requests.get("https://www.doviz.com/kripto-paralar/dogecoin", headers=header)
    veri = BeautifulSoup(e.content, "html.parser")
    veri = veri.find("div", {"class": "currency-card relative bg-blue-gray-9 rounded-md p-16"})
    doge = veri.find("div", {"class": "text-xl font-semibold text-white"}).text
    doge = doge[1:] # delete "$" first
    return doge

def ada():
    e = requests.get("https://www.doviz.com/kripto-paralar/cardano", headers=header)
    veri = BeautifulSoup(e.content, "html.parser")
    veri = veri.find("div", {"class": "currency-card relative bg-blue-gray-9 rounded-md p-16"})
    ada = veri.find("div", {"class": "text-xl font-semibold text-white"}).text
    ada = ada[1:] # delete "$" first
    return ada

def xrp():
    e = requests.get("https://www.doviz.com/kripto-paralar/ripple", headers=header)
    veri = BeautifulSoup(e.content, "html.parser")
    veri = veri.find("div", {"class": "currency-card relative bg-blue-gray-9 rounded-md p-16"})
    xrp = veri.find("div", {"class": "text-xl font-semibold text-white"}).text
    xrp = xrp[1:] # delete "$" first
    return xrp

def bnb():
    e = requests.get("https://www.doviz.com/kripto-paralar/binancecoin", headers=header)
    veri = BeautifulSoup(e.content, "html.parser")
    veri = veri.find("div", {"class": "currency-card relative bg-blue-gray-9 rounded-md p-16"})
    bnb = veri.find("div", {"class": "text-xl font-semibold text-white"}).text
    bnb = bnb[1:] # delete "$" first
    return bnb

def shiba():
    e = requests.get("https://www.doviz.com/kripto-paralar/shiba-inu", headers=header)
    veri = BeautifulSoup(e.content, "html.parser")
    veri = veri.find("div", {"class": "currency-card relative bg-blue-gray-9 rounded-md p-16"})
    shiba = veri.find("div", {"class": "text-xl font-semibold text-white"}).text
    shiba = shiba[1:] # delete "$" first
    return shiba
